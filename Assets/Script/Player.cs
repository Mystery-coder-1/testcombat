using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



public class Player : MonoBehaviour
{

    public EnemyData enemyData;
    public PlayerData playerData;
    private Animator animator;
    private static Dictionary<int, Action<Player>> actions = new Dictionary<int, Action<Player>>();

    public Animator Animator { get => animator; set => animator = value; }

    private void Awake()
    {
        animator = GetComponent<Animator>();
        playerData.hp = 100.0f;
        playerData.strength = 10.0f;
        playerData.energy = 10.0f;
        playerData.defense = 10.0f;
       
        InitActions();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.J))
        {
            DoCommand(0);
        }     
        if(Input.GetKeyDown(KeyCode.U))
        {
            DoCommand(1);
        }
        if(Input.GetKeyDown(KeyCode.I))
        {
            DoCommand(2);
        }
    }

    private void InitActions()
    {
        actions[0] = NormalAttack.Instance.AttackEnemy;
        actions[1] = WeaponSkiill.Instance.WeaponAttack;
        actions[2] = UniqueSkill.Instance.CastSkill;
    }

    private void DoCommand(int index)
    {
        if(actions.TryGetValue(index, out var func))
        {
            func(this);
        }
        else
        {
            return;
        }
    }
}
