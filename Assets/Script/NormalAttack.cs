using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAttack : Singleton<NormalAttack>
{

    public void AttackEnemy(Player player)
    {

        player.Animator.SetTrigger("Attack");
        player.enemyData.hp -= 20.0f;
        Debug.Log(player.enemyData.hp);

    }
}
