using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyDatas", menuName = "EnemyData/Skeleton", order = 1)]
public class EnemyData : ScriptableObject
{
    public float hp;
    public float strength;
    public float defense;

    
}
