using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerDatas", menuName = "PlayerData/Kiana", order = 2)]
public class PlayerData : ScriptableObject
{
    public float hp;
    public float energy;
    public float strength;
    public float defense;      
  
}
