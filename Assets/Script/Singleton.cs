using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Singleton<DT> where DT: class
{
    static DT instance;

    public static DT Instance => instance = instance ?? Activator.CreateInstance<DT>();


}
