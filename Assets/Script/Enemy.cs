using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{   
    private Animator animator;

    public EnemyData enemyData;

    public Animator Animator { get => animator; set => animator = value; }

    private void Awake()
    {       
        animator = GetComponent<Animator>();
        enemyData.hp = 100.0f;
        enemyData.strength = 10.0f;
        enemyData.defense = 10.0f;
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log(enemyData.hp);
        }
    }
}
