using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KianaData : PlayerData
{
    private static KianaData instance;

    public static KianaData Instance => instance = instance ?? Activator.CreateInstance<KianaData>();

    public void InitData(float pHp,float pEnergy, float pStrength, float pDefense)
    {
        hp = pHp;
        energy = pEnergy;
        strength = pStrength;
        defense = pDefense;
    }
}
